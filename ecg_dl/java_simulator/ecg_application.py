import ecg_params
import ecg_calc
# import matplotlib
# matplotlib.use('agg')
import matplotlib.pyplot as plt
import random
import os
import sys
path_to_generic_module = os.path.join('/', 'Users', 'tomer.golany', 'PycharmProjects', 'my_dl_lib')
sys.path.insert(0, path_to_generic_module)
import numpy as np
import generic_dataset
import generic_neural_network
import tensorflow as tf


class EcgApplication:

    def __init__(self):
        """

        """
        self.ecg_params_obj = ecg_params.EcgParam()
        self.ecg_calc_obj = ecg_calc.EcgCalc(self.ecg_params_obj)


def prepare_input_for_decoder(ecg_sequence, c_params, size_of_ecg_sequence=524, size_of_each_sample=5,
                              number_of_values_to_predict=5):
    """
    :param ecg_sequence: a sequence generated from the simulator. - [v1, v2, v3, ... , v_n]
    :param size_of_ecg_sequence: Length of the ecg_sequence. i.e: len(ecg_sequence)
    :param size_of_each_sample: We will slice the sequence to groups of "size_of_each_sample" -
            [[v1, v2, v3, v4, v5]
             [v6, v7, v8, v9, 10],
             ...
             [v(i), v(i+1), v(i+2), v(i+3), v(i+4)],
             ...
             [v(n-4), v(n-3), v(n-2), v(n-1), v(n)]]
    :param number_of_values_to_predict:
    :return: Labeled data as follows:
             Each "sample" in the list of samples will have a label which is the next sample:
             x[0] = [0, 0, 0, 0, 0]  labels[0] = [v1, v2, v3, v4, v5]
             x[1] = [v1, v2, v3, v4, v5]  labels[1] = [v6, v7, v8, v9, v10] ...
             Later we will concatenate each input with the 15 parameters vector C. ("The output from the encoder")
             Additional settings - number of values to predict at each time step:
    """
    # print("Length of the LSTM decoder will be: %d" % size_of_ecg_sequence)
    assert len(ecg_sequence) == size_of_ecg_sequence
    # Split into items were each item is of size sample.

    seq_splited_by_size_of_sample = [np.array(ecg_sequence[i * size_of_each_sample: (i + 1) * size_of_each_sample])
                                     for i in range(len(ecg_sequence) // size_of_each_sample)]

    '''
    start_loc = 0
    labeles = []

    seq_splited_by_size_of_sample = []
    seq_splited_by_size_of_sample.append([0, 0, 0, 0, 0])
    labeles.append([ecg_sequence[0]])
    seq_splited_by_size_of_sample.append([0, 0, 0, 0, ecg_sequence[0]])
    labeles.append([ecg_sequence[1]])
    seq_splited_by_size_of_sample.append([0, 0, 0, ecg_sequence[0], ecg_sequence[1]])
    labeles.append([ecg_sequence[2]])
    seq_splited_by_size_of_sample.append([0, 0, ecg_sequence[0], ecg_sequence[1], ecg_sequence[2]])
    labeles.append([ecg_sequence[3]])
    seq_splited_by_size_of_sample.append([0, ecg_sequence[0], ecg_sequence[1], ecg_sequence[2], ecg_sequence[3]])
    labeles.append([ecg_sequence[4]])
    while start_loc + size_of_each_sample < len(ecg_sequence):
        seq_splited_by_size_of_sample.append(np.array(ecg_sequence[start_loc: start_loc + size_of_each_sample]))
        labeles.append(ecg_sequence[
                       start_loc + size_of_each_sample: start_loc + size_of_each_sample + number_of_values_to_predict])
        start_loc += number_of_values_to_predict
    '''
    # This is one sequence sample (which is made of "size_of_ecg_sequence" "words" (i.e samples) the wee feed into the
    # LSTM)
    x = [np.concatenate((sample, c_params)) for sample in seq_splited_by_size_of_sample]
    # The labels for this specific sample is determined from number_of_values_to_predict shifted one to the left
    labeles = seq_splited_by_size_of_sample
    # [0, 0, 0, 0, 0]  --> this is the "starting sentence"
    # TODO: generic imp
    '''
    if number_of_values_to_predict == 1:
        for i in range(size_of_each_sample):
            prefix = np.concatenate((np.array([0 for _ in range(size_of_each_sample - i)]), x[0][:i]))
            x = np.concatenate((np.concatenate((np.array(prefix), c_params)).reshape(1, size_of_each_sample +
                                                                                              len(c_params)),
                                np.array(x)))
    '''
    # else:
    x = np.concatenate((np.concatenate((np.array([0 for _ in range(size_of_each_sample)]), c_params)).reshape(1, size_of_each_sample +
                                                                                      len(c_params)), np.array(x)))
    # remove from x the last sample becasue it doesnt have a label:
    x = x[:-1]
    return x, labeles


def create_decoder(number_of_hidden_neurons, size_of_ecg_sequence, size_of_each_sample, x_train, y_train, x_val, y_val,
                   x_test, y_test, num_of_iterations=100, batch_size=150, model_name='decoder',
                   number_of_values_to_predict=5):
    """
    :param: number_of_hidden_neurons - number of neurons at each gate of the LSTM cell which defines the neuron
    :param: decoder_emb_inp -
    :param: decoder_lengths -
    :return:
    """

    tensor_board_logs_dir = os.path.join('.', 'logs', 'decoder')
    saved_models_dir = os.path.join('.', 'saved_models', model_name)
    # initial the placeholder which will hold a batch of params - C.
    number_of_params_to_equation = 15
    # input_params = tf.placeholder("float", [None, number_of_params_to_equation])
    # Build RNN cell
    decoder_cell = tf.contrib.rnn.MultiRNNCell([tf.contrib.rnn.BasicLSTMCell(number_of_hidden_neurons)
                                                for _ in range(0, 2)])
    # decoder_cell = tf.nn.rnn_cell.BasicLSTMCell(number_of_hidden_neurons)

    # tf Graph input
    # + 1 because of the "starting word"
    # x_input = tf.placeholder("float", [None, int(size_of_ecg_sequence/size_of_each_sample) + 1, size_of_each_sample +
    #                                   number_of_params_to_equation])
    x_input = tf.placeholder("float", [None, None, size_of_each_sample +
                                       number_of_params_to_equation], name='x_input')

    # y = tf.placeholder("float", [None, int(size_of_ecg_sequence/size_of_each_sample), size_of_each_sample],
    #                    name='y_output')
    y = tf.placeholder("float", [None, int(size_of_ecg_sequence/number_of_values_to_predict), number_of_values_to_predict],
                       name='y_output')

    # split the placeholder to a list of T tensors, each tensor is all the "words/samples" of a single time step.
    # x = tf.unstack(x_input, num=int(size_of_ecg_sequence/size_of_each_sample) + 1, axis=1)

    # Generate prediction:
    # outputs is a length T list of outputs (one for each input), or a nested tuple of such elements.
    # state is the final state
    # outputs, state = tf.contrib.rnn.static_rnn(decoder_cell, x, dtype=tf.float32)
    # defining initial state
    # b_size = tf.shape(x_data)[0]
    # initial_state = decoder_cell.zero_state(batch_size, dtype=tf.float32)
    # Reminder: Becasue we are using LSTM cell, then it has the S hidden state and the C hidden state.
    outputs, last_state = tf.nn.dynamic_rnn(cell=decoder_cell, inputs=x_input,
                                            dtype=tf.float32)
    # Shape of each output is []
    shape_of_one_output = outputs[-1].get_shape().as_list()
    print("Shape of one output: ", shape_of_one_output)
    # Generate the final output for each output from outputs. each output should be multiplied with the same weights.
    with tf.name_scope('output_layer'):
        v_w = tf.Variable(tf.random_normal((shape_of_one_output[1], number_of_values_to_predict)))
        v_b = tf.Variable(tf.random_normal([number_of_values_to_predict]))
        variable_summaries(v_w)
        variable_summaries(v_b)
        # v = tf.layers.dense(outputs, size_of_each_sample, name='v')
    # reshape to (batch_size * num_steps, hidden_size)
    transformed_outputs = tf.reshape(outputs, [-1, shape_of_one_output[1]])
    v = tf.nn.xw_plus_b(transformed_outputs, v_w, v_b, name='v')
    # Reshape logits to be a 3-D tensor for sequence loss
    reshaped_v = tf.reshape(v, [-1, int(size_of_ecg_sequence/number_of_values_to_predict), number_of_values_to_predict])
    # transformed_outputs = [tf.matmul(output, v_w) + v_b for output in outputs]
    # Drop the last output. in this case we don't care about him.
    # transformed_outputs = transformed_outputs[:-1]
    # concatenated_outputs = tf.concat(axis=1, values=transformed_outputs)
    # concatenated_outputs = tf.stack(transformed_outputs, axis=1)
    # Training:
    cost = tf.reduce_mean(tf.squared_difference(reshaped_v, y))
    tf.summary.scalar('mean_square_loss', cost)
    optimizer = tf.train.RMSPropOptimizer(learning_rate=0.001).minimize(cost)

    # Initializing the variables
    init = tf.global_variables_initializer()

    # Create a saver object which will save all the variables
    saver = tf.train.Saver()

    # Merge all the summaries and write them out:
    merged_summaries = tf.summary.merge_all()

    # Launch the graph
    session = tf.Session()
    session.run(init)

    # Create Writer object to visualize the graph later:
    train_writer = tf.summary.FileWriter(tensor_board_logs_dir + '/train', session.graph)
    val_writer = tf.summary.FileWriter(tensor_board_logs_dir + '/val')

    loss_total = 0
    data_iterator = generic_dataset.GenericDataSetIterator(x_train, y_train)
    for i in range(num_of_iterations):
        x_batch, y_batch = data_iterator.next_batch(batch_size)

        summary, _, loss = session.run([merged_summaries, optimizer, cost], feed_dict={x_input: x_batch, y: y_batch})
        loss_total += loss
        train_writer.add_summary(summary, i)

        if (i + 1) % 10 == 0:
            print("Step:%d   train_loss:%.6f" % (i + 1, loss))
            val_summary, val_loss = session.run([merged_summaries, cost], feed_dict={x_input: x_val, y: y_val})
            print('step %d, val loss %g' % (i + 1, val_loss))
            val_writer.add_summary(val_summary, i)
            # Save the graph
            saver.save(session, os.path.join(saved_models_dir, 'decoder_ode'), global_step=i)

    # Now, save the graph
    saver.save(session, os.path.join(saved_models_dir, 'decoder_ode'), global_step=num_of_iterations)

    # Prediction stage:

    # test_params, test_y = create_randomally_data_points(1)
    test_params = [1.2, 0.25, -60, -5, 0.1, -15, 30, 0.1, 0, -7.5, 0.1, 15, 0.75, 0.4, 90]
    test_params = np.array(test_params).reshape(1, 15)
    ecg_app = EcgApplication()
    ecg_app.ecg_params_obj.N = 2
    ecg_app.ecg_calc_obj.calculate_ecg()
    test_y = ecg_app.ecg_calc_obj.ecg_result_voltage

    test_x = np.concatenate((np.array([0 for _ in range(size_of_each_sample)]),
                             test_params[0])).reshape(1, 1, number_of_params_to_equation + size_of_each_sample)
    # Get the output for the first time step
    test_outputs = session.run(v, feed_dict={x_input: test_x})

    # Editing for size one output:
    test_outputs = np.concatenate((test_outputs[-1], test_params[0])).reshape(1, 1,
                                                                              number_of_params_to_equation +
                                                                              size_of_each_sample)
    next_input = np.concatenate((test_x, test_outputs), axis=1)
    '''
    test_outputs = session.run(v, feed_dict={x_input: next_input})
    test_outputs = np.concatenate((np.array([0, 0, 0, test_outputs[-2][0], test_outputs[-1][0]]).reshape(1, 5), test_params[0])).reshape(
        1, 1,
        number_of_params_to_equation +
        size_of_each_sample)

    next_input = np.concatenate((next_input, test_outputs), axis=1)
    test_outputs = session.run(v, feed_dict={x_input: next_input})
    test_outputs = np.concatenate(
        (np.array([0, 0, test_outputs[-3][0], test_outputs[-2][0], test_outputs[-1][0]]).reshape(1, 5), test_params[0])).reshape(
        1, 1,
        number_of_params_to_equation +
        size_of_each_sample)

    next_input = np.concatenate((next_input, test_outputs), axis=1)
    test_outputs = session.run(v, feed_dict={x_input: next_input})
    test_outputs = np.concatenate(
        (np.array([0, test_outputs[-4][0], test_outputs[-3][0], test_outputs[-2][0], test_outputs[-1][0]]).reshape(1, 5), test_params[0])).reshape(
        1, 1,
        number_of_params_to_equation +
        size_of_each_sample)
    next_input = np.concatenate((next_input, test_outputs), axis=1)
    '''
    test_output = None
    for _ in range(1, int(size_of_ecg_sequence/size_of_each_sample)):
        test_output = session.run(v, feed_dict={x_input: next_input})
        next_input = test_x
        for i in range(test_output.shape[0]):
        # for i in range(test_output.shape[1]):
            # word = test_output[:, i, :]
            word = test_output[i, :]
            word_with_params = np.concatenate((word.reshape(size_of_each_sample), test_params[0])).reshape(1,
                                                                                                           1,
                                                                                                           number_of_params_to_equation
                                                                                                           + size_of_each_sample)
            next_input = np.concatenate((next_input, word_with_params), axis=1)

    test_output = test_output.reshape(int(size_of_ecg_sequence/size_of_each_sample) * size_of_each_sample)
    plt.figure()
    plt.plot(test_output)
    plt.figure()
    plt.plot(test_y)
    plt.show()

    print("Output from LSTM: ", test_output)
    print("Output from simulator: ", test_y)
    print("Calculate square difference:")
    simulator_place_holder = tf.placeholder('float', shape=int(size_of_ecg_sequence/size_of_each_sample) * size_of_each_sample)
    decoder_placeholder = tf.placeholder('float', shape=int(size_of_ecg_sequence/size_of_each_sample) * size_of_each_sample)
    err_tensor = tf.reduce_mean(tf.squared_difference(simulator_place_holder, decoder_placeholder))
    err = session.run(err_tensor, feed_dict={simulator_place_holder: test_y[:int(size_of_ecg_sequence/size_of_each_sample) * size_of_each_sample], decoder_placeholder: test_output})
    print(err)

    session.close()


def decoder_model(model_name):
    """
    :param model_name: name of model when we save checkpoint files.
    :return:
    """
    # Generate params and ecg signals from the simulator:
    print("Randomizing data points from the ECG simulator:")
    c_params, ecg_signals = create_randomally_data_points(200)
    print("Finished to randomized data from simulator")
    x_data = []
    y_data = []
    for i, signal in enumerate(ecg_signals):
        x, labeles = prepare_input_for_decoder(signal, c_params[i], size_of_ecg_sequence=524, size_of_each_sample=5,
                                               number_of_values_to_predict=5)
        x_data.append(x)
        y_data.append(labeles)

    # Seperate to train, test and validation sets:
    x_train = x_data[:int(0.8 * len(x_data))]
    y_train = y_data[:int(0.8 * len(y_data))]
    x_test = x_data[int(0.8 * len(x_data)):]
    y_test = y_data[int(0.8 * len(y_data)):]
    x_val = x_train[int(0.8 * len(x_train)):]
    y_val = y_train[int(0.8 * len(y_train)):]
    x_train = x_train[:int(0.8 * len(x_train))]
    y_train = y_train[:int(0.8 * len(y_train))]
    print("Lengh of x train: %d . Length of y train %d" % (len(x_train), len(y_train)))
    print("Lengh of x val: %d . Length of y val %d" % (len(x_val), len(y_val)))
    print("Lengh of x test: %d . Length of y test %d" % (len(x_test), len(y_test)))
    create_decoder(512, 524, 5, np.array(x_train), np.array(y_train), np.array(x_val), np.array(y_val),
                   np.array(x_test), np.array(y_test), num_of_iterations=10, batch_size=150, model_name=model_name,
                   number_of_values_to_predict=5)


def load_trained_decoder(decoder_file, size_of_each_sample, size_of_ecg_sequence):
    """

    :param decoder_file:
    :return:
    """
    number_of_params_to_equation = 15
    with tf.Session() as sess:
        saver = tf.train.import_meta_graph(decoder_file + '.meta')
        saver.restore(sess, tf.train.latest_checkpoint('./'))
        graph = tf.get_default_graph()
        a = [n.name for n in graph.as_graph_def().node]
        print(a)
        v = graph.get_tensor_by_name("xw_plus_b:0")
        # tags_placeholder = graph.get_tensor_by_name("y_output:0")
        x_input = graph.get_tensor_by_name("x_input:0")

        # Prediction stage:
        # test_params, test_y = create_randomally_data_points(1)
        test_params = [1.2, 0.25, -60, -5, 0.1, -15, 30, 0.1, 0, -7.5, 0.1, 15, 0.75, 0.4, 90]
        test_params = np.array(test_params).reshape(1, 15)
        ecg_app = EcgApplication()
        ecg_app.ecg_params_obj.N = 2
        ecg_app.ecg_calc_obj.calculate_ecg()
        test_y = ecg_app.ecg_calc_obj.ecg_result_voltage

        test_x = np.concatenate((np.array([0 for _ in range(size_of_each_sample)]), test_params[0])).reshape(1, 1,
                                                                                                             number_of_params_to_equation +
                                                                                                             size_of_each_sample)
        # Get the output for the first time step
        test_outputs = sess.run(v, feed_dict={x_input: test_x})
        test_outputs = np.concatenate((test_outputs[-1], test_params[0])).reshape(1, 1, number_of_params_to_equation +
                                                                                                             size_of_each_sample)
        next_input = np.concatenate((test_x, test_outputs), axis=1)
        test_output = None
        for _ in range(1, int(size_of_ecg_sequence/size_of_each_sample)):
            test_output = sess.run(v, feed_dict={x_input: next_input})
            next_input = test_x
            for i in range(test_output.shape[0]):
                word = test_output[i, :]
                word_with_params = np.concatenate((word.reshape(size_of_each_sample), test_params[0])).reshape(1, 1, number_of_params_to_equation +
                                                                                                             size_of_each_sample)
                next_input = np.concatenate((next_input, word_with_params), axis=1)

        test_output = test_output.reshape(int(size_of_ecg_sequence/size_of_each_sample) * size_of_each_sample)
        plt.figure()
        plt.plot(test_output)
        plt.figure()
        plt.plot(test_y)
        plt.show()

        print("Output from LSTM: ", test_output)
        print("Output from simulator: ", test_y)
        print("Calculate square difference:")
        simulator_place_holder = tf.placeholder('float', shape=size_of_ecg_sequence)
        decoder_placeholder = tf.placeholder('float', shape=size_of_ecg_sequence)
        err_tensor = tf.reduce_mean(tf.squared_difference(simulator_place_holder, decoder_placeholder))
        err = sess.run(err_tensor, feed_dict={simulator_place_holder: test_y, decoder_placeholder: test_output})
        print(err)


def variable_summaries(var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)
        tf.summary.scalar('max', tf.reduce_max(var))
        tf.summary.scalar('min', tf.reduce_min(var))
        tf.summary.histogram('histogram', var)


def create_randomally_data_points(number_of_points_to_randomize=5000):
    """
    Randomize values for the parameters of the ECG simulators and output signal from the simulator for each
    randomization.
    :param: number_of_points_to_randomize - the number of points we want to randomize.
    :return: returns a dictionary where each key is a set of randomized values of the parameters and the value is the
    corresponding ECG output from the simulator.
    """
    ecg_app = EcgApplication()
    ecg_app.ecg_calc_obj.calculate_ecg()
    x_train = []
    y_train = []
    for i in range(number_of_points_to_randomize):
        if i % 10 == 0:
            print("Randomization iteration %d" % i)
        p_a = random.uniform(-30, 30)
        p_b = random.uniform(-30, 30)
        p_theta = random.uniform(-180, 180)
        # print("P values: %f, %f, %f" % (p_a, p_b, p_theta))

        q_a = random.uniform(-30, 30)
        q_b = random.uniform(-30, 30)
        q_theta = random.uniform(-180, 180)
        # print("Q values: %f, %f, %f" % (q_a, q_b, q_theta))

        r_a = random.uniform(-30, 30)
        r_b = random.uniform(-30, 30)
        r_theta = random.uniform(-180, 180)
        # print("R values: %f, %f, %f" % (r_a, r_b, r_theta))

        s_a = random.uniform(-30, 30)
        s_b = random.uniform(-30, 30)
        s_theta = random.uniform(-180, 180)
        # print("S values: %f, %f, %f" % (s_a, s_b, s_theta))

        t_a = random.uniform(-30, 30)
        t_b = random.uniform(-30, 30)
        t_theta = random.uniform(-180, 180)
        # print("T values: %f, %f, %f" % (t_a, t_b, t_theta))

        ecg_app.ecg_params_obj.theta = [p_theta, q_theta, r_theta, s_theta, t_theta]
        ecg_app.ecg_params_obj.a = [p_a, q_a, r_a, s_a, t_a]
        ecg_app.ecg_params_obj.b = [p_b, q_b, r_b, s_b, t_b]
        ecg_app.ecg_params_obj.N = 2
        ecg_app.ecg_calc_obj.calculate_ecg()
        resulted_voltage = ecg_app.ecg_calc_obj.ecg_result_voltage

        # plt.plot(resulted_voltage)
        '''
        rpeaks = ecg_app.ecg_calc_obj.ecg_result_peaks
        peak_locations = [i for i, e in enumerate(rpeaks) if e == 3]
        assert len(peak_locations) == 2
        peak_location = peak_locations[1]
        start = peak_location - 120
        if start < 0:
            start = 0
        end = peak_location + 120
        # end = min(end, len(ecg_signal) - 1)
        if end > len(resulted_voltage) - 1:
            end = len(resulted_voltage) - 1
        beat = []
        for i in range(start, end):
            beat.append(resulted_voltage[i])
        '''
        params = [p_a, p_b, p_theta, q_a, q_b, q_theta, r_a, r_b, r_theta, s_a, s_b, s_theta, t_a, t_b, t_theta]
        x_train.append(params)
        y_train.append(resulted_voltage)
    return np.array(x_train), np.array(y_train)


def create_data_points_deterministic(number_of_points=50000):
    """
    Randomize values for the parameters of the ECG simulators and output signal from the simulator for each
    randomization.
    :param: number_of_points - the number of points we want to randomize.
    :return: returns a dictionary where each key is a set of randomized values of the parameters and the value is the
    corresponding ECG output from the simulator.
    """
    ecg_app = EcgApplication()
    ecg_app.ecg_calc_obj.calculate_ecg()
    ecg_app.ecg_params_obj.N = 2
    x_train = []
    y_train = []
    count = number_of_points
    for p_a in np.arange(-30.0,31.0, 0.1):
        for p_b in np.arange(-30.0,31.0, 0.1):
            for p_theta in np.arange(-180.0, 180.1, 0.1):
                for q_a in np.arange(-30.0, 31.0, 0.1):
                    for q_b in np.arange(-30.0, 31.0, 0.1):
                        for q_theta in np.arange(-180.0, 180.1, 0.1):
                            for r_a in np.arange(-30.0, 31.0, 0.1):
                                for r_b in np.arange(-30.0, 31.0, 0.1):
                                    for r_theta in np.arange(-180.0, 180.1, 0.1):
                                        for s_a in np.arange(-30.0, 31.0, 0.1):
                                            for s_b in np.arange(-30.0, 31.0, 0.1):
                                                for s_theta in np.arange(-180.0, 180.1, 0.1):
                                                    for t_a in np.arange(-30.0, 31.0, 0.1):
                                                        for t_b in np.arange(-30.0, 31.0, 0.1):
                                                            for t_theta in np.arange(-180.0, 180.1, 0.1):


                                                                ecg_app.ecg_calc_obj.calculate_ecg()
    resulted_voltage = ecg_app.ecg_calc_obj.ecg_result_voltage

    # plt.plot(resulted_voltage)
    rpeaks = ecg_app.ecg_calc_obj.ecg_result_peaks
    peak_locations = [i for i, e in enumerate(rpeaks) if e == 3]
    assert len(peak_locations) == 2
    peak_location = peak_locations[1]
    start = peak_location - 120
    if start < 0:
        start = 0
    end = peak_location + 120
    # end = min(end, len(ecg_signal) - 1)
    if end > len(resulted_voltage) - 1:
        end = len(resulted_voltage) - 1
    beat = []
    for i in range(start, end):
        beat.append(resulted_voltage[i])
    params = [p_a, p_b, p_theta, q_a, q_b, q_theta, r_a, r_b, r_theta, s_a, s_b, s_theta, t_a, t_b, t_theta]
    x_train.append(params)
    y_train.append(beat)
    return np.array(x_train), np.array(y_train)


def create_approximation_function():
    """

    :return:
    """
    x, y = create_randomally_data_points()
    x_train = x[:int(0.8 * len(x))]
    y_train = y[:int(0.8 * len(y))]
    x_test = x[int(0.8 * len(x)):]
    y_test = y[int(0.8 * len(y)):]
    x_val = x_train[int(0.8 * len(x_train)):]
    y_val = y_train[int(0.8 * len(y_train)):]
    x_train = x_train[:int(0.8 * len(x_train))]
    y_train = y_train[:int(0.8 * len(y_train))]
    print("Lengh of x train: %d . Length of y train %d" % (len(x_train), len(y_train)))
    print("Lengh of x val: %d . Length of y val %d" % (len(x_val), len(y_val)))
    print("Lengh of x test: %d . Length of y test %d" % (len(x_test), len(y_test)))
    arch_file = os.path.join("arch_files", 'approx_func_arch')
    apx_function = generic_neural_network.GenericNeuralNetwork(arch_file, './logs')
    apx_function.train_tf(x_train, y_train, x_val, y_val, 10000, 150, "adam", "mean_square")
    apx_function.eval_lost(x_test, y_test)
    print("DONE")


def test_apx_func():

    with tf.Session() as sess:
        # Restore variables from disk.
        saver = tf.train.import_meta_graph("ode_ecg_generator_aprox-10000.meta")
        saver.restore(sess, tf.train.latest_checkpoint('./'))
        print("Model restored.")
        # Check the values of the variables
        graph = tf.get_default_graph()
        a = [n.name for n in graph.as_graph_def().node]
        print(a)
        input_placeholder = graph.get_tensor_by_name("input_placeholder:0")
        output = graph.get_tensor_by_name('danse_layer_2/layer_2/BiasAdd:0')
        keepprob = graph.get_tensor_by_name('danse_layer_2/dropout/Placeholder:0')
        # x_test = [1.2, 0.25, -60, -5, 0.1, -15, 30, 0.1, 0, -7.5, 0.1, 15, 0.75, 0, 0]
        # x_test = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        x_test = [1.2, 0.25, -60, -5, 0.1, -15, 30, 0.1, 0, -7.5, 0.1, 15, 0.75, 0.4, 90]
        x_test = np.array(x_test).reshape(1, 15)
        res = sess.run(output, feed_dict={input_placeholder: x_test, keepprob: 1})
        plt.plot(res[0])
        plt.show()
        print(res[0])


if __name__ == "__main__":
    # test_apx_func()
    # create_approximation_function()
    # create_randomally_data_points()
    # print("DONE")

    decoder_model('decoder_with_sample_of_size_5_and_preiction_of_one_batch_150_rand_5000_iters_10')
    # load_trained_decoder('decoder_ode-1002', size_of_ecg_sequence=524, size_of_each_sample=1)

    # ecg_app = EcgApplication()

    # print("Starting to work on ECG generation...")

    # ecg_app.ecg_params_obj.N = 2
    # ecg_app.ecg_params_obj.theta[0] = 0.0
    '''
    ecg_app.ecg_params_obj.theta[1] = 0.0
    ecg_app.ecg_params_obj.theta[2] = 0.0
    ecg_app.ecg_params_obj.theta[3] = 0.0
    ecg_app.ecg_params_obj.theta[4] = 0.0

    ecg_app.ecg_params_obj.a[0] = 0.0
    ecg_app.ecg_params_obj.a[1] = 0.0
    ecg_app.ecg_params_obj.a[2] = 0.0
    ecg_app.ecg_params_obj.a[3] = 0.0
    ecg_app.ecg_params_obj.a[4] = 0.0

    ecg_app.ecg_params_obj.b[0] = 0.0
    ecg_app.ecg_params_obj.b[1] = 0.0
    ecg_app.ecg_params_obj.b[2] = 0.0
    ecg_app.ecg_params_obj.b[3] = 0.0
    ecg_app.ecg_params_obj.b[4] = 0.0
    '''
    '''
    ecg_app.ecg_calc_obj.calculate_ecg()
    resulted_voltage = ecg_app.ecg_calc_obj.ecg_result_voltage
    # plt.plot(resulted_voltage)
    rpeaks = ecg_app.ecg_calc_obj.ecg_result_peaks
    peak_locations = [i for i, e in enumerate(rpeaks) if e == 4]
    print(len(peak_locations))
    print(resulted_voltage)
    print(len(resulted_voltage))
    plt.plot(resulted_voltage)
    plt.plot(peak_locations[1], 1, 'ro')
    plt.show()
    print(rpeaks)
    peak_location = peak_locations[1]
    start = peak_location - 120
    if start < 0:
        start = 0
    end = peak_location + 120
    # end = min(end, len(ecg_signal) - 1)
    if end > len(resulted_voltage) - 1:
        end = len(resulted_voltage) - 1
    beat = []
    for i in range(start, end):
        beat.append(resulted_voltage[i])
    print(beat)
    '''
    '''
    for i in range(10):
        print(ecg_app.ecg_calc_obj.ecg_result_time[i])
        print(ecg_app.ecg_calc_obj.ecg_result_voltage[i])
    '''
    # plt.plot(ecg_app.ecg_calc_obj.ecg_result_voltage[1:2500])
    # plt.show()
    # print("Finished working on ECG generation...")
