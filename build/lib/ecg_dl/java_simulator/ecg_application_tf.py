import ecg_params_tf
import ecg_calc_tf
import matplotlib.pyplot as plt


class EcgApplication:

    def __init__(self):
        """

        """
        self.ecg_params_obj = ecg_params_tf.EcgParam()
        self.ecg_calc_obj = ecg_calc_tf.EcgCalc(self.ecg_params_obj)


if __name__ == "__main__":
    ecg_app = EcgApplication()

    print("Starting to work on ECG generation...")

    ecg_app.ecg_calc_obj.calculate_ecg()

    '''
    for i in range(10):
        print(ecg_app.ecg_calc_obj.ecg_result_time[i])
        print(ecg_app.ecg_calc_obj.ecg_result_voltage[i])
    '''
    plt.plot(ecg_app.ecg_calc_obj.ecg_result_voltage[1:2500])
    plt.show()
    print("Finished working on ECG generation...")